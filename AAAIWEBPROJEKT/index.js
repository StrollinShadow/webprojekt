import express from "express";
import mongoose from "mongoose";
import profileRouter from "./backend/profile/profile.routes.js";
import cardioRouter from "./backend/cardio/cardio.routes.js";
import powerRouter from "./backend/power/power.routes.js";

const app = express();

mongoose.connect("mongodb://127.0.0.1:27017/KalorienTracker");

app.use(express.json());
app.use(express.static("frontend"));
app.use("/api/profiles", profileRouter);
app.use("/api/cardio", cardioRouter);
app.use("/api/power", powerRouter);

mongoose.connection.once("open", () => {
  console.log("Connected to MongoDB");
  app.listen(3001, () => {
    console.log("Server listening on http://localhost:3001");
  });
});
