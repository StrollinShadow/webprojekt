import cardioService from "./cardioService.js";
import powerService from "./powerService.js";
import profileService from "./profileService.js";

const userId = "65a14f045fbcfe10154a8d27";
function showContentSection(sectionId) {
  const sections = [
    "home-content",
    "cardio-content",
    "krafttraining-content",
    "profile-content",
  ];
  sections.forEach((section) => {
    document.getElementById(section).style.display =
      section === sectionId ? "block" : "none";
  });
}

// Initialize the page with default content
function initializePage() {
  showContentSection("home-content"); // Show home content by default
  attachNavbarEventListeners(); // Attach event listeners to navbar items
}

// Attach event listeners to navbar items
function attachNavbarEventListeners() {
  document
    .getElementById("back-button")
    .addEventListener("click", () => showContentSection("home-content"));
  document
    .getElementById("cardioCard")
    .addEventListener("click", () => showContentSection("cardio-content"));
  document
    .getElementById("krafttrainingCard")
    .addEventListener("click", () =>
      showContentSection("krafttraining-content")
    );
  document
    .getElementById("profile-icon")
    .addEventListener("click", () => showContentSection("profile-content"));
}

// Call initializePage when the DOM is fully loaded
document.addEventListener("DOMContentLoaded", initializePage);

// Attach event listener to the cardio form button
document
  .getElementById("calculate-cardio-calories")
  .addEventListener("click", (event) => {
    event.preventDefault(); // Prevent the default form submission

    // Gather input values
    const exercise = document.querySelector(
      "#cardio-content select#exercise-select option:checked"
    ).innerHTML;
    const distance = document.getElementById("distance-input").value;
    const time = document.getElementById("time-input").value;
    console.log(exercise);

    // Create a new cardio object
    const newCardioEntry = {
      exercise: exercise,
      distance: parseFloat(distance),
      time: parseFloat(time),
    };
    console.log(newCardioEntry);

    // Send the new cardio entry to the backend
    cardioService
      .create(newCardioEntry)
      .then((response) => {
        console.log("Cardio entry created successfully:", response);
        // Optionally, update the UI here
      })
      .catch((error) => {
        console.error("Error creating cardio entry:", error);
      });
  });

document
  .getElementById("calculate-training-calories")
  .addEventListener("click", (event) => {
    event.preventDefault(); // Prevent the default form submission

    // Gather input values
    const exercise = document.querySelector(
      "#krafttraining-content select#exercise-select option:checked"
    ).innerHTML;
    const reps = document.getElementById("reps-input").value;
    const weight = document.getElementById("weight-input").value;

    // Create a new strength training object
    const newTrainingEntry = {
      exercise: exercise,
      reps: parseFloat(reps),
      weight: parseFloat(weight),
    };

    // Send the new strength training entry to the backend
    powerService
      .create(newTrainingEntry)
      .then((response) => {
        console.log("Training entry created successfully:", response);
        // Optionally, update the UI here
      })
      .catch((error) => {
        console.error("Error creating training entry:", error);
      });
  });

document.getElementById("profile-form").addEventListener("submit", (event) => {
  event.preventDefault(); // Prevent the default form submission

  // Gather input values
  const weight = document.getElementById("weight").value;
  const height = document.getElementById("height").value;
  const gender = document.getElementById("gender").value;

  const userId = "65a14f045fbcfe10154a8d27"; // Example user ID, replace with actual
  const profileUpdate = {
    weight: parseFloat(weight),
    height: parseFloat(height),
    gender,
  };

  profileService
    .update(userId, profileUpdate)
    .then((response) => {
      console.log("Profile updated successfully:", response);
      // Optionally, update the UI here
    })
    .catch((error) => {
      console.error("Error updating profile:", error);
    });
});

function getProfileData() {
  return profileService
    .getAll() // Corrected this line
    .then((profiles) => {
      if (profiles.length > 0) {
        return profiles[0]; // Assuming you want the first profile
      } else {
        throw new Error("No profiles found");
      }
    })
    .catch((error) => {
      console.error("Error fetching profile data:", error);
      throw error; // Rethrow error to handle it in the calling function
    });
}

// Example usage
getProfileData()
  .then((profile) => {
    console.log("Retrieved profile:", profile);
    // Here you can use the profile data, e.g., profile.weight
  })
  .catch((error) => {
    // Handle error, e.g., show an error message to the user
  });

function calculateCardioCalories(exercise, weight, duration) {
  const MET_VALUES = {
    //Met Values sind wie anstregend eine Aktivität ist
    running: 9.8,
    cycling: 6.6,
    swimming: 8.3,
    jogging: 5.1,
    rowing: 5.9,
  };

  const metValue = MET_VALUES[exercise];
  if (!metValue) {
    console.error("Exercise not recognized");
    return 0;
  }

  // Calculate calories burned
  return metValue * weight * (duration / 60); // Convert duration to hours
}

// Attach event listener to the cardio form button
document
  .getElementById("calculate-cardio-calories")
  .addEventListener("click", (event) => {
    event.preventDefault(); // Prevent the default form submission

    // Gather input values
    const exercise = document.getElementById("exercise-select").value;
    const duration = parseFloat(document.getElementById("time-input").value); // Assuming duration is in minutes

    // Fetch user profile data and calculate calories
    getProfileData()
      .then((profile) => {
        const caloriesBurned = calculateCardioCalories(
          exercise,
          profile.weight,
          duration
        );
        alert(
          ` ${caloriesBurned.toFixed(2)} Kalorien wurden verbrannt. Weiter so!`
        );
        console.log(
          `Calories burned for ${duration} minutes of ${exercise}: ${caloriesBurned.toFixed(
            2
          )} calories`
        );
      })
      .catch((error) => {
        console.error("Error fetching profile data:", error);
      });
  });

// Function to calculate calories for strength training exercises
function calculateStrengthCalories(userWeight, exerciseWeight, duration, reps) {
  const BASE_MET = 3; // Base MET value for moderate intensity strength training
  const WEIGHT_FACTOR = 0.005; // Factor to adjust calorie burn per kg of exercise weight
  const REP_FACTOR = 0.1; // Factor to adjust calorie burn per repetition

  // Adjust MET value based on the weight used in the exercise
  const adjustedMET =
    BASE_MET + exerciseWeight * WEIGHT_FACTOR + reps * REP_FACTOR;

  // Calculate calories burned
  // MET value * user's weight in kg * duration in hours
  return adjustedMET * userWeight * (duration / 60);
}

// Attach event listener to the strength training form button
document
  .getElementById("calculate-training-calories")
  .addEventListener("click", (event) => {
    event.preventDefault(); // Prevent the default form submission

    // Gather input values
    const exerciseWeight = parseFloat(
      document.getElementById("weight-input").value
    );
    const reps = parseFloat(document.getElementById("reps-input").value);
    const duration = parseFloat(document.getElementById("time-input").value); // Assuming duration is in minutes

    // Fetch user profile data and calculate calories
    getProfileData()
      .then((profile) => {
        const caloriesBurned = calculateStrengthCalories(
          profile.weight,
          exerciseWeight,
          duration,
          reps
        );
        alert(
          ` ${caloriesBurned.toFixed(2)} Kalorien wurden verbrannt. Weiter so!`
        );

        console.log(
          `Calories burned for ${reps} reps of strength training: ${caloriesBurned.toFixed(
            2
          )} calories`
        );
      })
      .catch((error) => {
        console.error("Error fetching profile data:", error);
      });
  });
