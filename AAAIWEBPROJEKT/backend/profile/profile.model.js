import mongoose from "mongoose";

const profileSchema = new mongoose.Schema({
  weight: Number,
  height: Number,
  gender: String,
});

const model = mongoose.model("Profile", profileSchema);

export { model };
