import { Router } from "express";
import { getProfiles, updateProfile } from "./profile.controller.js";

const router = Router();

router.get("/", getProfiles);
router.put("/:id", updateProfile);

export default router;
