import { model as Profile } from "./profile.model.js";

async function getProfiles(request, response) {
  await Profile.find()
    .exec()
    .then((profiles) => {
      response.json(profiles);
    })
    .catch((error) => {
      response.status(500).json({ message: error.message });
    });
}

async function updateProfile(request, response) {
  const profileId = request.params.id;
  await Profile.findById(profileId)
    .exec()
    .then((profile) => {
      if (!profile) {
        return Promise.reject({
          message: `Profile with id ${profileId} not found.`,
          status: 404,
        });
      }
      const providedProfile = request.body;
      profile.weight = providedProfile.weight;
      profile.height = providedProfile.height;
      profile.gender = providedProfile.gender;
      return profile.save();
    })
    .then((updatedProfile) => {
      response.json(updatedProfile);
    })
    .catch((error) => {
      if (error.status) {
        response.status(error.status);
      } else {
        response.status(500);
      }
      response.json({ message: error.message });
    });
}

export { getProfiles, updateProfile };
