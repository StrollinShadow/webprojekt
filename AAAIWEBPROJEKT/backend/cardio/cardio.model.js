import mongoose from "mongoose";

const cardioSchema = new mongoose.Schema({
  exercise: String,
  distance: Number,
  time: Number,
});

const model = mongoose.model("Cardio", cardioSchema);

export { model };
