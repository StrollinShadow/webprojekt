import { Router } from "express";
import { getCardios, createCardio, updateCardio } from "./cardio.controller.js";

const router = Router();

router.get("/", getCardios);
router.post("/", createCardio);
router.put("/:id", updateCardio);

export default router;
