import { model as Cardio } from "./cardio.model.js";

async function getCardios(request, response) {
  await Cardio.find()
    .exec()
    .then((cardios) => {
      response.json(cardios);
    })
    .catch((error) => {
      response.status(500);
      response.json({ message: error.message });
    });
}

async function createCardio(request, response) {
  const newCardio = request.body;
  await Cardio.create({
    exercise: newCardio.exercise,
    distance: newCardio.distance,
    time: newCardio.time,
  })
    .then((savedCardio) => {
      response.json(savedCardio);
    })
    .catch((error) => {
      response.status(500);
      response.json({ message: error.message });
    });
}

async function updateCardio(request, response) {
  const cardioId = request.params.id;
  await Cardio.findById(cardioId)
    .exec()
    .then((cardio) => {
      if (!cardio) {
        return Promise.reject({
          message: `Cardio with id ${cardioId} not found.`,
          status: 404,
        });
      }
      const providedCardio = request.body;
      cardio.exercise = providedCardio.exercise;
      cardio.distance = providedCardio.distance;
      cardio.time = providedCardio.time;
      return cardio.save();
    })
    .then((updatedCardio) => {
      response.json(updatedCardio);
    })
    .catch((error) => {
      if (error.status) {
        response.status(error.status);
      } else {
        response.status(500);
      }
      response.json({ message: error.message });
    });
}

export { getCardios, createCardio, updateCardio };
