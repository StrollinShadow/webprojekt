import {
  getCardios,
  createCardio,
  updateCardio,
  // Add any other methods you might have
} from "../cardio/cardio.controller.js";
import { model as Cardio } from "../cardio/cardio.model.js";

jest.mock("../cardio/cardio.model.js");

const response = {
  status: jest.fn((x) => x),
  json: jest.fn((x) => x),
};

describe("Cardio Controller Tests", () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  // Test: Loading cardios successfully
  test("Loading cardios successfully", async () => {
    const storedCardios = [
      { id: "1", exercise: "Running", distance: 5, time: 30 },
      { id: "2", exercise: "Cycling", distance: 20, time: 60 },
      // ... more test data
    ];
    Cardio.find.mockImplementation(() => {
      return {
        exec: jest.fn().mockResolvedValue(storedCardios),
      };
    });

    await getCardios(null, response);
    expect(response.json).toHaveBeenCalledWith(storedCardios);
  });

  // Test: Loading cardios unsuccessfully
  test("Loading cardios unsuccessfully", async () => {
    Cardio.find.mockImplementation(() => {
      return {
        exec: jest.fn().mockRejectedValue({ message: "DB error" }),
      };
    });

    await getCardios(null, response);
    expect(response.status).toHaveBeenCalledWith(500);
    expect(response.json).toHaveBeenCalledWith({ message: "DB error" });
  });

  // Test: Creating cardio successfully
  test("Creating cardio successfully", async () => {
    const request = {
      body: {
        exercise: "Swimming",
        distance: 2,
        time: 30,
      },
    };
    const newCardio = { id: "3", ...request.body };
    Cardio.create.mockResolvedValue(newCardio);

    await createCardio(request, response);
    expect(response.json).toHaveBeenCalledWith(newCardio);
  });

  // Test: Creating cardio unsuccessfully
  test("Creating cardio unsuccessfully", async () => {
    const request = {
      body: {
        exercise: "Swimming",
        distance: 2,
        time: 30,
      },
    };
    Cardio.create.mockRejectedValue(new Error("DB error"));

    await createCardio(request, response);
    expect(response.status).toHaveBeenCalledWith(500);
    expect(response.json).toHaveBeenCalledWith({ message: "DB error" });
  });

  describe("Updating cardios", () => {
    const mockRequest = {
      params: { id: "1" },
      body: {
        exercise: "Updated Exercise",
        distance: 10,
        time: 40,
      },
    };

    test("Updating cardio successfully", async () => {
      const cardioToUpdate = {
        id: "1",
        save: jest.fn().mockResolvedValue({
          id: "1",
          ...mockRequest.body,
        }),
      };
      Cardio.findById.mockImplementation(() => {
        return {
          exec: jest.fn().mockResolvedValue(cardioToUpdate),
        };
      });

      await updateCardio(mockRequest, response);
      expect(cardioToUpdate.save).toHaveBeenCalled();
      expect(response.json).toHaveBeenCalledWith({
        id: "1",
        ...mockRequest.body,
      });
    });

    test("Updating cardio fails because id is missing", async () => {
      Cardio.findById.mockImplementation(() => {
        return {
          exec: jest.fn().mockResolvedValue(null),
        };
      });

      await updateCardio(mockRequest, response);
      expect(response.status).toHaveBeenCalledWith(404);
      expect(response.json).toHaveBeenCalledWith({
        message: "Cardio with id 1 not found.",
      });
    });

    // ... additional test scenarios for updateCardio ...
  });

  describe("Deleting cardios", () => {
    const mockRequest = {
      params: { id: "1" },
    };

    test("Deleting cardio successfully", async () => {
      const deletedCardio = {
        id: "1",
        exercise: "Deleted Exercise",
        distance: 0,
        time: 0,
      };
      Cardio.findById.mockImplementation(() => {
        return {
          exec: jest.fn().mockResolvedValue({
            deleteOne: jest.fn().mockResolvedValue(deletedCardio),
          }),
        };
      });

      await removeCardio(mockRequest, response);
      expect(response.json).toHaveBeenCalledWith(deletedCardio);
    });

    test("Deleting cardio fails because id is missing", async () => {
      Cardio.findById.mockImplementation(() => {
        return {
          exec: jest.fn().mockResolvedValue(null),
        };
      });

      await removeCardio(mockRequest, response);
      expect(response.status).toHaveBeenCalledWith(404);
      expect(response.json).toHaveBeenCalledWith({
        message: "Cardio with id 1 not found.",
      });
    });
  });
});
