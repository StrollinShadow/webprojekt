import { Router } from "express";
import {
  getPowers,
  createPower,
  updatePower,
  deletePower,
} from "./power.controller.js";

const router = Router();

router.get("/", getPowers);
router.post("/", createPower);
router.put("/:id", updatePower);
router.delete("/:id", deletePower);

export default router;
