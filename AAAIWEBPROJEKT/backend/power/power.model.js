import mongoose from "mongoose";

const powerSchema = new mongoose.Schema({
  exercise: String,
  reps: Number,
  weight: Number,
});

const model = mongoose.model("Power", powerSchema);

export { model };
