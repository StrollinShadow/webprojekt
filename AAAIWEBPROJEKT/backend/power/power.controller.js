import { model as Power } from "./power.model.js";

async function getPowers(request, response) {
  await Power.find()
    .exec()
    .then((powers) => {
      response.json(powers);
    })
    .catch((error) => {
      response.status(500).json({ message: error.message });
    });
}

async function createPower(request, response) {
  const newPower = request.body;
  await Power.create({
    exercise: newPower.exercise,
    reps: newPower.reps,
    weight: newPower.weight,
  })
    .then((savedPower) => {
      response.json(savedPower);
    })
    .catch((error) => {
      response.status(400).json({ message: error.message });
    });
}

async function updatePower(request, response) {
  const powerId = request.params.id;
  await Power.findById(powerId)
    .exec()
    .then((power) => {
      if (!power) {
        return Promise.reject({
          message: `Power with id ${powerId} not found.`,
          status: 404,
        });
      }
      const providedPower = request.body;
      power.exercise = providedPower.exercise;
      power.reps = providedPower.reps;
      power.weight = providedPower.weight;
      return power.save();
    })
    .then((updatedPower) => {
      response.json(updatedPower);
    })
    .catch((error) => {
      if (error.status) {
        response.status(error.status);
      } else {
        response.status(500);
      }
      response.json({ message: error.message });
    });
}

async function deletePower(request, response) {
  const powerId = request.params.id;
  await Power.findByIdAndDelete(powerId)
    .exec()
    .then((deletedPower) => {
      if (!deletedPower) {
        return response
          .status(404)
          .json({ message: `Power with id ${powerId} not found.` });
      }
      response.json({ message: `Power with id ${powerId} has been deleted.` });
    })
    .catch((error) => {
      response.status(500).json({ message: error.message });
    });
}

export { getPowers, createPower, updatePower, deletePower };
